# ProtocolTool

#### 介绍
一款可配置的协议解析工具

#### 软件架构
基于 Qt 的协议解析工具，可以用 json 配置协议解析方法，用 js 文件加载 crc 计算方法。



#### 数据解析

利用 json 文件即可配置协议解析方式，比如解析 modbus 数据：

```json
{
"data":[
    {"desc":"从机地址","start":0,"length":1,"precision":1,"offset":0,"unit":"","data_type":1},
    {"desc":"功能码","start":0,"length":1,"precision":1,"offset":0,"unit":"","data_type":1},
    {"desc":"寄存器地址","start":0,"length":2,"precision":1,"offset":0,"unit":"","data_type":1},
    {"desc":"寄存器数量","start":0,"length":2,"precision":1,"offset":0,"unit":"","data_type":1},
    {"desc":"CRC","start":0,"length":2,"precision":1,"offset":0,"unit":"","data_type":1}
]
}
```

使用前可以用 json 格式化工具查看是否有错误。

json 的键表示的含义：

- desc 数据的描述
- start 数据的起始位(实际并没有用到，填入0即可)
- length 数据长度，字节长度，如果是 byte 就是 1，word 就是 2，字符串就是字符串长度
- precision 精度，不用的时候填 1
- offset 偏移，不用的时候填 0 利用精度和偏移计算浮点数，实际值 = 数据包中的值 * 精度 + 偏移
- unit 数据的单位
- data_type 数据的类型 0-string 1-byte/word/dword 3-byte[n]

json 文件采用分段的方式解析协议，用户需要填写每一个数据的长度，然后软件会根据输入的字符串进行解析。

#### CRC 校验

软件可以利用外部 js 脚本来计算 CRC ，用户需要在程序运行目录下面创建一个 default.js 文件，然后写入对应的 CRC 计算函数，需要注意的是，软件传入 js 的参数是十六进制字符串，在 js 脚本中需要先解析，具体可以看 default.js 文件。

下面是计算 modbus crc16 的一个例子

```js
function HexStr2Bytes(str)

{
    var pos = 0;

    var len = str.length;

    if(len %2 != 0)

    {
       return null; 

    }

    len /= 2;

    var hexA = new Array();

    for(var i=0; i<len; i++)

    {
       var s = str.substr(pos, 2);

       var v = parseInt(s, 16);

       hexA.push(v);

       pos += 2;

    }

    return hexA;

}


function modbus_crc16(str)
{
  var hexA = HexStr2Bytes(str);
  var crc = 0xffff;
  for (i = 0; i < hexA.length-2; i++)
  {
    crc=crc ^ hexA[i];
    for ( j=0; j<8; j++)
    {
        if( ( crc&0x0001) >0)
        {
            crc=crc>>1;
            crc=crc^ 0xa001;
        }
        else
            crc=crc>>1;
    }
  }
  var crc_h = (crc)&0xff;
  var crc_l = (crc>>8)&0xff;
  crc = crc_h<<8 | crc_l;

  return crc;
}
```

用户需要将对应的函数名填入到软件中，点击开始解析后就可以计算 CRC 和解析数据。



#### 暂未解决的问题

数据预览的窗口，使用 crtl + 鼠标滚轮进行放大的缩小的时候有一些不协调。

在软件中修改 json 功能暂未开放，用户可以自行修改。



