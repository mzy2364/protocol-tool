
function HexStr2Bytes(str)

{
    var pos = 0;

    var len = str.length;

    if(len %2 != 0)

    {
       return null; 

    }

    len /= 2;

    var hexA = new Array();

    for(var i=0; i<len; i++)

    {
       var s = str.substr(pos, 2);

       var v = parseInt(s, 16);

       hexA.push(v);

       pos += 2;

    }

    return hexA;

}


function modbus_crc16(str)
{
  var hexA = HexStr2Bytes(str);
  var crc = 0xffff;
  for (i = 0; i < hexA.length-2; i++)
  {
    crc=crc ^ hexA[i];
    for ( j=0; j<8; j++)
    {
        if( ( crc&0x0001) >0)
        {
            crc=crc>>1;
            crc=crc^ 0xa001;
        }
        else
            crc=crc>>1;
    }
  }
  var crc_h = (crc)&0xff;
  var crc_l = (crc>>8)&0xff;
  crc = crc_h<<8 | crc_l;

  return crc;
}

function gb17691_crc(str)
{
  var hexA = HexStr2Bytes(str);
  var value = hexA[2];
  for (i = 3; i < hexA.length-1; i++)
  {
  	value = value ^ hexA[i];
  }
  return value;
}