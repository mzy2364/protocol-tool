#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QByteArray>
#include <QString>
#include <QDebug>
#include <QStringList>
#include <QList>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QScrollBar>
#include <QScriptValueList>
#include <QScriptEngine>


namespace Ui {
class MainWindow;
}

typedef int (*func_type)(int a,int b);
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    QByteArray HexStringToByteArray(QString HexString);     //十六进制的字符串转bytearray

    QByteArray input_data;  //保存转换后的待解析数据

    QStringList output_data;    //输出解析后数据显示

    /* 单个数据的描述 */
    typedef struct{
        QString desc;       //数据的描述
        int start;          //起始字节
        int length;         //数据长度
        double precision;    //精度
        int offset;         //偏移
        QString unit;       //单位
        int data_type;      //数据类型 0-字符 1-二进制 2-二进制数组
    }DATA_PAR_T;

    QList<DATA_PAR_T> external_data_psrsing;    //当前使用的外部协议

    QStringList data_parsing(QByteArray &,QList<DATA_PAR_T> list,bool disp_hex);    //数据解析函数

    QByteArray application_file_data;   //程序初始化json文件的数据
    void init_protocol_list();  //初始化协议列表 从文件加载
    void init_protocol_list(QByteArray);  //初始化协议列表 从bytearray加载
    QStringList protocol_file_path_list;    //保存协议的json文件路径

    //导入协议
    bool import_protocol(QString file_path, QList<DATA_PAR_T> &data_psrsing);

    //调用外部js脚本计算CRC
    //uint32_t check_crc(QString hex_str,QString func_name);
    QString check_crc_from_js(QString hex_str,QString func_name);



private:
    bool eventFilter(QObject *watched, QEvent *event);
    bool ctrl_key_press = false;
protected:
//     virtual void keyPressEvent(QKeyEvent *event);      //键盘按下事件
//     virtual void keyReleaseEvent(QKeyEvent *event);    //键盘释放事件
//     virtual void wheelEvent(QWheelEvent *event);       //鼠标滚轮事件

private slots:
    void on_pushButton_start_clicked();

    void on_pushButton_import_protocol_clicked();

    void on_pushButton_clear_clicked();

    void on_comboBox_protocol_type_activated(int index);

    void on_pushButton_remove_clicked();


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
